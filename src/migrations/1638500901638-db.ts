import {MigrationInterface, QueryRunner} from "typeorm";

export class db1638500901638 implements MigrationInterface {
    name = 'db1638500901638'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` CHANGE \`stock\` \`stock\` int NOT NULL DEFAULT '9999'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` CHANGE \`stock\` \`stock\` int NOT NULL`);
    }

}
