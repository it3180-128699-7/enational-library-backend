import {MigrationInterface, QueryRunner} from "typeorm";

export class db1637595772027 implements MigrationInterface {
    name = 'db1637595772027'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` CHANGE \`author\` \`authorId\` varchar(255) NOT NULL`);
        await queryRunner.query(`CREATE TABLE \`author\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(100) NOT NULL, \`avatar\` varchar(255) NOT NULL, \`bio\` tinytext NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`book\` DROP COLUMN \`authorId\``);
        await queryRunner.query(`ALTER TABLE \`book\` ADD \`authorId\` int NULL`);
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`password\``);
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`password\` varchar(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`book\` ADD CONSTRAINT \`FK_66a4f0f47943a0d99c16ecf90b2\` FOREIGN KEY (\`authorId\`) REFERENCES \`author\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` DROP FOREIGN KEY \`FK_66a4f0f47943a0d99c16ecf90b2\``);
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`password\``);
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`password\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`book\` DROP COLUMN \`authorId\``);
        await queryRunner.query(`ALTER TABLE \`book\` ADD \`authorId\` varchar(255) NOT NULL`);
        await queryRunner.query(`DROP TABLE \`author\``);
        await queryRunner.query(`ALTER TABLE \`book\` CHANGE \`authorId\` \`author\` varchar(255) NOT NULL`);
    }

}
