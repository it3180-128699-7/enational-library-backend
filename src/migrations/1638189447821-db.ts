import {MigrationInterface, QueryRunner} from "typeorm";

export class db1638189447821 implements MigrationInterface {
    name = 'db1638189447821'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`order_book\` (\`rentedTime\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`receivedTime\` datetime NULL, \`expirationTime\` datetime NULL, \`returnedTime\` datetime NULL, \`orderId\` int NOT NULL, \`bookId\` int NOT NULL, PRIMARY KEY (\`orderId\`, \`bookId\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`order\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(100) NOT NULL, \`address\` varchar(255) NOT NULL, \`phone\` varchar(255) NOT NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`userId\` int NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`order_book\` ADD CONSTRAINT \`FK_f96d43406bfd1a292368827a166\` FOREIGN KEY (\`orderId\`) REFERENCES \`order\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`order_book\` ADD CONSTRAINT \`FK_8eb568d1e2869b005d598d597cc\` FOREIGN KEY (\`bookId\`) REFERENCES \`book\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`order\` ADD CONSTRAINT \`FK_caabe91507b3379c7ba73637b84\` FOREIGN KEY (\`userId\`) REFERENCES \`user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`order\` DROP FOREIGN KEY \`FK_caabe91507b3379c7ba73637b84\``);
        await queryRunner.query(`ALTER TABLE \`order_book\` DROP FOREIGN KEY \`FK_8eb568d1e2869b005d598d597cc\``);
        await queryRunner.query(`ALTER TABLE \`order_book\` DROP FOREIGN KEY \`FK_f96d43406bfd1a292368827a166\``);
        await queryRunner.query(`DROP TABLE \`order\``);
        await queryRunner.query(`DROP TABLE \`order_book\``);
    }

}
