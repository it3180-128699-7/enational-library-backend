import {MigrationInterface, QueryRunner} from "typeorm";

export class db1638629640480 implements MigrationInterface {
    name = 'db1638629640480'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`order_book\` CHANGE \`status\` \`status\` enum ('pending', 'received', 'returning', 'returned') NOT NULL DEFAULT 'pending'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`order_book\` CHANGE \`status\` \`status\` enum ('pedning', 'received', 'returning', 'returned') NOT NULL DEFAULT 'pedning'`);
    }

}
