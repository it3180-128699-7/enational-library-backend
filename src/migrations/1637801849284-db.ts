import {MigrationInterface, QueryRunner} from "typeorm";

export class db1637801849284 implements MigrationInterface {
    name = 'db1637801849284'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`feedback\` ADD \`name\` varchar(100) NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`feedback\` DROP COLUMN \`name\``);
    }

}
