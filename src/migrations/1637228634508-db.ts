import {MigrationInterface, QueryRunner} from "typeorm";

export class db1637228634508 implements MigrationInterface {
    name = 'db1637228634508'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`user_rents_book\` (\`rentedTime\` datetime NOT NULL, \`receivedTime\` datetime NULL, \`expirationTime\` datetime NULL, \`returnedTime\` datetime NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`userId\` int NOT NULL, \`bookId\` int NOT NULL, PRIMARY KEY (\`userId\`, \`bookId\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`book\` ADD \`image\` varchar(500) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`user_rents_book\` ADD CONSTRAINT \`FK_7f95e23f073873ae706344d607a\` FOREIGN KEY (\`userId\`) REFERENCES \`user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`user_rents_book\` ADD CONSTRAINT \`FK_684e27ad6bf0082d4a09f8a54bc\` FOREIGN KEY (\`bookId\`) REFERENCES \`book\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user_rents_book\` DROP FOREIGN KEY \`FK_684e27ad6bf0082d4a09f8a54bc\``);
        await queryRunner.query(`ALTER TABLE \`user_rents_book\` DROP FOREIGN KEY \`FK_7f95e23f073873ae706344d607a\``);
        await queryRunner.query(`ALTER TABLE \`book\` DROP COLUMN \`image\``);
        await queryRunner.query(`DROP TABLE \`user_rents_book\``);
    }

}
