import {MigrationInterface, QueryRunner} from "typeorm";

export class db1637235039255 implements MigrationInterface {
    name = 'db1637235039255'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` ADD \`author\` varchar(255) NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` DROP COLUMN \`author\``);
    }

}
