import {MigrationInterface, QueryRunner} from "typeorm";

export class db1639386104263 implements MigrationInterface {
    name = 'db1639386104263'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`account\` ADD \`foulTimes\` int NOT NULL DEFAULT '0'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`account\` DROP COLUMN \`foulTimes\``);
    }

}
