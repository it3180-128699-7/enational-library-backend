import {MigrationInterface, QueryRunner} from "typeorm";

export class db1638500752981 implements MigrationInterface {
    name = 'db1638500752981'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` ADD \`stock\` int NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` DROP COLUMN \`stock\``);
    }

}
