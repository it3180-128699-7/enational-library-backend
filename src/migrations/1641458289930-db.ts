import {MigrationInterface, QueryRunner} from "typeorm";

export class db1641458289930 implements MigrationInterface {
    name = 'db1641458289930'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` ADD \`deletedAt\` datetime(6) NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` DROP COLUMN \`deletedAt\``);
    }

}
