import {MigrationInterface, QueryRunner} from "typeorm";

export class db1637653757990 implements MigrationInterface {
    name = 'db1637653757990'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`category\` DROP COLUMN \`code\``);
        await queryRunner.query(`ALTER TABLE \`book\` ADD \`sku\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`book\` ADD UNIQUE INDEX \`IDX_8cfc1cf40b239a856b577bdb95\` (\`sku\`)`);
        await queryRunner.query(`ALTER TABLE \`book\` ADD \`isPublished\` tinyint NOT NULL DEFAULT 0`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` DROP COLUMN \`isPublished\``);
        await queryRunner.query(`ALTER TABLE \`book\` DROP INDEX \`IDX_8cfc1cf40b239a856b577bdb95\``);
        await queryRunner.query(`ALTER TABLE \`book\` DROP COLUMN \`sku\``);
        await queryRunner.query(`ALTER TABLE \`category\` ADD \`code\` varchar(255) NULL`);
    }

}
