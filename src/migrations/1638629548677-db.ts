import {MigrationInterface, QueryRunner} from "typeorm";

export class db1638629548677 implements MigrationInterface {
    name = 'db1638629548677'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`order_book\` ADD \`status\` enum ('pedning', 'received', 'returning', 'returned') NOT NULL DEFAULT 'pedning'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`order_book\` DROP COLUMN \`status\``);
    }

}
