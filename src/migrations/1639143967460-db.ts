import {MigrationInterface, QueryRunner} from "typeorm";

export class db1639143967460 implements MigrationInterface {
    name = 'db1639143967460'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`account\` ADD \`isActive\` tinyint NOT NULL DEFAULT 1`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`account\` DROP COLUMN \`isActive\``);
    }

}
