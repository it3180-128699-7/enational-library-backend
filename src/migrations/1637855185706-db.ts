import {MigrationInterface, QueryRunner} from "typeorm";

export class db1637855185706 implements MigrationInterface {
    name = 'db1637855185706'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`account\` CHANGE \`balance\` \`balance\` double NOT NULL DEFAULT '0'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`account\` CHANGE \`balance\` \`balance\` double NOT NULL`);
    }

}
