import expressLoader from './express';
import dependencyInjectorLoader from './dependencyInjector';
import databaseLoader from './database';
import Logger from './logger';
import { User } from '@src/entities/User';
import { Book } from '@src/entities/Book';
import { Account } from '@src/entities/Account';
import { Author } from '@src/entities/Author';
import { Category } from '@src/entities/Category';
import { OrderBook } from '@src/entities/OrderBook';
import { Feedback } from '@src/entities/Feedback';
import { Order } from '@src/entities/Order';
export default async ({ expressApp }) => {
  const runMode = process.env.NODE_ENV;
  Logger.info(`✅ Server is running on [${runMode.toUpperCase()}] mode.`);

  const connection = await databaseLoader.create();
  Logger.info('✅ DB loaded and connected!');

  await dependencyInjectorLoader({
    repositories: [User, Account, Book, Author, Category, Order, OrderBook, Feedback].map(e => ({
      name: e.name.charAt(0).toLowerCase() + e.name.slice(1) + 'Repository',
      repository: connection.manager.getRepository(e),
    })),
  });
  Logger.info('✅ Dependency Injector loaded.');

  await expressLoader({ app: expressApp });
  Logger.info('✅ Express loaded.');
};
