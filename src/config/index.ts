import * as dotenv from 'dotenv';
import { Algorithm as IJWTAlgorithm } from 'jsonwebtoken';

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config();
if (envFound.error) {
  // This error should crash whole process
  throw new Error("⚠️ Couldn't find .env file ⚠️");
}

export default {
  /**
   * Server config
   */
  port: parseInt(process.env.PORT, 10),
  host: process.env.HOST,

  /**
   * Your secret sauce
   */
  jwtSecret: process.env.JWT_SECRET,
  jwtAlgorithm: (process.env.JWT_ALGO as IJWTAlgorithm) || 'HS256',
  jwtExpireTimeNormal: process.env.JWT_EXPIRE_NORMAL,
  jwtExpireTimeLong: process.env.JWT_EXPIRE_LONG,

  /**
   * Database config
   */
  orm: {
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT, 10),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    testDatabase: process.env.DB_TEST_NAME,
  },

  /**
   * Used by winston logger
   */
  logs: {
    level: process.env.LOG_LEVEL || 'silly',
  },
  /**
   * API configs
   */
  api: {
    prefix: process.env.API_PREFIX || 'api',
  },
  /**
   * Mailgun email credentials
   */
  emails: {
    apiKey: process.env.MAILGUN_API_KEY,
    apiUsername: process.env.MAILGUN_USERNAME,
    domain: process.env.MAILGUN_DOMAIN,
  },

  /**
   * Cloudinary config
   */
  cloudinary: {
    name: process.env.CLOUDINARY_NAME,
    apiKey: process.env.CLOUDINARY_API_KEY,
    apiSecret: process.env.CLOUDINARY_API_SECRET,
  },

  /**
   * App constants
   */
  appConstants: {
    RENEWAL_NORMAL_USER_MONTH: Number.parseFloat(process.env.RENEWAL_NORMAL_USER_MONTH) || 2,
    RENEWAL_VIP_USER_MONTH: Number.parseFloat(process.env.RENEWAL_VIP_USER_MONTH) || 3,
    DISCOUNT_VIP_USER: Number.parseFloat(process.env.DISCOUNT_VIP_USER) || 0.1,
    DISCOUNT_NORMAL_USER: Number.parseFloat(process.env.DISCOUNT_NORMAL_USER) || 0.05,

    MAX_BOOK_NORMAL_USER: Number.parseFloat(process.env.MAX_BOOK_NORMAL_USER) || 5,
    MAX_BOOK_VIP_USER: Number.parseFloat(process.env.MAX_BOOK_VIP_USER) || 8,
    RENT_TIME_VIP: Number.parseFloat(process.env.RENT_TIME_VIP) || 65,
    RENT_TIME_NORMAL: Number.parseFloat(process.env.RENT_TIME_NORMAL) || 50,
    LATE_DAYS: Number.parseFloat(process.env.LATE_DAYS) || 5,
  },
};
