import { Service, Inject, Container } from 'typedi';
import { Repository } from 'typeorm';

import { BadRequestException, NotFoundException } from '@src/utils/CustomError';
import { Book } from '@src/entities/Book';
import { CreateBookDto, GetAllBooksParamsDto, SortParams, UpdateBookDto } from '@src/dto/book.dto';
import { convertDto } from '@src/utils/common';
import { Author } from '@src/entities/Author';
import { Category } from '@src/entities/Category';
import { OrderBook } from '@src/entities/OrderBook';
import { PaginationDto } from '@src/dto/pagination.dto';
import PaginationService from './pagination.service';

@Service()
export default class BookService {
  constructor(
    @Inject('bookRepository') private bookRepository: Repository<Book>,
    @Inject('authorRepository') private authorRepository: Repository<Author>,
    @Inject('categoryRepository') private categoryRepository: Repository<Category>,
    private readonly paginationService = Container.get(PaginationService),
  ) {}

  public async findAll(
    params: GetAllBooksParamsDto,
    canViewNoPublished: boolean,
  ): Promise<PaginationDto<Book>> {
    const { page, limit, search, sort, exclusive, author, category } = params;

    const qb = this.bookRepository
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.category', 'category')
      .leftJoinAndSelect('book.author', 'author')
      .leftJoinAndMapMany(
        'popular',
        subQuery => {
          return subQuery
            .select('orderBook.bookId', 'bookId')
            .addSelect('count(*)', 'popular')
            .from(OrderBook, 'orderBook')
            .groupBy('orderBook.bookId');
        },
        'popular',
        'book.id = popular.bookId',
      )
      .take(limit)
      .skip((page - 1) * limit)
      .orderBy(SortParams[sort][0], SortParams[sort][1])
      .addOrderBy('book.id', 'DESC');

    if (search) {
      qb.andWhere('book.title LIKE :search', { search: `%${search}%` });
    }
    // filter category
    if (category) {
      qb.andWhere('category.id = :categoryId', { categoryId: category });
    }
    // filter author
    if (author) {
      qb.andWhere('author.id = :authorId', { authorId: author });
    }
    // filter exclusive
    // 1: only exclusive books
    // 2: only normal books
    // other: all books
    switch (exclusive) {
      case 1:
        qb.andWhere('book.isExclusive = true');
        break;
      case 2:
        qb.andWhere('book.isExclusive = false');
        break;
    }
    // if is admin
    if (!canViewNoPublished) {
      qb.andWhere('book.isPublished = true');
    }

    const result = await qb.getManyAndCount();
    return this.paginationService.paginate(params, result[0], result[1]);
  }

  public async findOne(id: number | string, canViewNoPublished: boolean): Promise<Book> {
    const qb = this.bookRepository
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.category', 'category')
      .leftJoinAndSelect('book.author', 'author')
      .where('book.id = :id', { id });

    if (!canViewNoPublished) {
      qb.andWhere('book.isPublished = true');
    }
    const book = await qb.getOne();

    if (!book) {
      throw new NotFoundException('findOneBook', 'Book not found');
    }
    return book;
  }

  public async create(createBookDto: CreateBookDto): Promise<Book> {
    // check if sku of book exist
    const isExisted = await this.bookRepository.findOne({ sku: createBookDto.sku });
    if (isExisted) {
      throw new BadRequestException('createBook', 'SKU already exists');
    }

    // insert category
    const category = await this.categoryRepository.findOne(createBookDto.category);
    if (!category) {
      throw new NotFoundException('createBook', 'Category not found');
    }

    // insert author
    const author = await this.authorRepository.findOne(createBookDto.author);
    if (!author) {
      throw new NotFoundException('createBook', 'Author not found');
    }

    const newBook = new Book();
    convertDto(createBookDto, newBook);
    newBook.author = author;
    newBook.category = category;

    return this.bookRepository.save(newBook);
  }

  public async update(id: number | string, updateBookDto: UpdateBookDto): Promise<Book> {
    // findBook
    const book = await this.bookRepository.findOne(id);
    if (!book) {
      throw new NotFoundException('updateBook', 'Book not found');
    }

    // check if sku of book exist
    if (updateBookDto.sku !== book.sku) {
      const isExisted = await this.bookRepository.findOne({ sku: updateBookDto.sku });
      if (isExisted) {
        throw new BadRequestException('updateBook', 'SKU already exists');
      }
    }

    // reinsert category
    const category = await this.categoryRepository.findOne(updateBookDto.category);
    if (!category) {
      throw new NotFoundException('createBook', 'Category not found');
    }

    // reinsert author
    const author = await this.authorRepository.findOne(updateBookDto.author);
    if (!author) {
      throw new NotFoundException('createBook', 'Author not found');
    }
    convertDto(updateBookDto, book);
    book.author = author;
    book.category = category;

    return this.bookRepository.save(book);
  }

  public async delete(id: number | string): Promise<any> {
    const book = await this.bookRepository.findOne(id);
    if (!book) {
      throw new NotFoundException('deleteBook', 'Book not found');
    }
    return this.bookRepository.softDelete(id);
  }
}
