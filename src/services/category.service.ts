import { Service, Inject } from 'typedi';
import { Repository } from 'typeorm';

import { NotFoundException } from '@src/utils/CustomError';
import { Category } from '@src/entities/Category';
import { CreateCategoryDto, UpdateCategoryDto } from '@src/dto/category.dto';
import { convertDto } from '@src/utils/common';

@Service()
export default class CategoryService {
  constructor(@Inject('categoryRepository') private categoryRepository: Repository<Category>) {}

  public async findAll(): Promise<Category[]> {
    return this.categoryRepository.find();
  }

  public async findOne(id: number | string): Promise<Category> {
    const category = await this.categoryRepository.findOne(id);
    if (!category) {
      throw new NotFoundException('findOneCategory', 'Category not found');
    }
    return category;
  }

  public async create(createCategoryDto: CreateCategoryDto): Promise<Category> {
    const newCategory = new Category();
    convertDto(createCategoryDto, newCategory);
    return this.categoryRepository.save(newCategory);
  }

  public async update(
    id: number | string,
    updateCategoryDto: UpdateCategoryDto,
  ): Promise<Category> {
    const category = await this.categoryRepository.findOne(id);
    if (!category) {
      throw new NotFoundException('updateCategory', 'Category not found');
    }
    convertDto(updateCategoryDto, category);
    return this.categoryRepository.save(category);
  }

  public async delete(id: number | string): Promise<any> {
    const category = await this.categoryRepository.findOne(id);
    if (!category) {
      throw new NotFoundException('deleteCategory', 'Category not found');
    }
    return this.categoryRepository.delete(id);
  }
}
