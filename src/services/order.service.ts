import { Service, Inject, Container } from 'typedi';
import { Brackets, Repository } from 'typeorm';
import { compareAsc, add, differenceInDays } from 'date-fns';
import * as mysql from 'mysql';

import { BadRequestException, NotFoundException } from '@src/utils/CustomError';
import { Book } from '@src/entities/Book';
import { User } from '@src/entities/User';
import { Order } from '@src/entities/Order';
import { OrderBook, OrderBookStatus } from '@src/entities/OrderBook';
import {
  CreateOrderDto,
  SortParams,
  GetAllOrdersParamsDto,
  ReturnedBooksDto,
  ReturningOrderDto,
} from '@src/dto/order.dto';
import { Account } from '@src/entities/Account';
import config from '@src/config';
import { PaginationDto } from '@src/dto/pagination.dto';
import PaginationService from './pagination.service';

const { MAX_BOOK_NORMAL_USER, MAX_BOOK_VIP_USER, RENT_TIME_NORMAL, RENT_TIME_VIP, LATE_DAYS } =
  config.appConstants;

@Service()
export default class OrderService {
  constructor(
    @Inject('accountRepository') private accountRepository: Repository<Account>,
    @Inject('bookRepository') private bookRepository: Repository<Book>,
    @Inject('orderRepository') private orderRepository: Repository<Order>,
    @Inject('orderBookRepository') private orderBookRepository: Repository<OrderBook>,
    private readonly paginationService = Container.get(PaginationService),
  ) {}

  public async create(user: User, createOrder: CreateOrderDto): Promise<Order> {
    const books = await this.bookRepository
      .createQueryBuilder('book')
      .where('book.id IN (:...books)', { books: createOrder.books })
      .getMany();

    if (books.length < createOrder.books.length) {
      throw new NotFoundException('create order', 'Book not found');
    }

    const countRentedBook = await this.orderBookRepository
      .createQueryBuilder('order_book')
      .leftJoinAndSelect('order_book.order', 'order')
      .leftJoinAndSelect('order.user', 'user')
      .leftJoinAndSelect('order_book.book', 'book')
      .where('user.id=:userId', { userId: user.id })
      .andWhere('book.id IN (:...bookIds)', { bookIds: createOrder.books })
      .andWhere('order_book.returnedTime is null')
      .getCount();

    if (countRentedBook > 0) {
      throw new BadRequestException('create order', "You've already rent some books");
    }

    const vipUser = user.account.isVip;

    const numOfBook = books.length;
    //count how many book have rented
    const totalrentedBook = await this.orderBookRepository
      .createQueryBuilder('order_book')
      .leftJoinAndSelect('order_book.order', 'order')
      .leftJoinAndSelect('order.user', 'user')
      .where('user.id=:userId', { userId: user.id })
      .andWhere('order_book.returnedTime is null')
      .getCount();
    const MAX_BOOK = vipUser ? MAX_BOOK_VIP_USER : MAX_BOOK_NORMAL_USER;
    if (numOfBook + totalrentedBook > MAX_BOOK) {
      throw new BadRequestException('create order', 'Too much book');
    }

    for (let i = 0; i < books.length; i++) {
      if (books[i].isExclusive) {
        if (!vipUser) throw new BadRequestException('create order', 'You cannot rent this book');
      }

      if (books[i].stock === 0) {
        throw new BadRequestException('create order', 'Book not available');
      }
    }

    //check expirationtime of account
    const now = new Date();
    const expTime = user.account.expirationTime;
    const compare = compareAsc(now, expTime);
    if (compare === 1) {
      throw new BadRequestException('create order', 'Your account is expired');
    }

    let newOrder = new Order();
    newOrder.orderBooks = [];
    newOrder.name = createOrder.name;
    newOrder.address = createOrder.address;
    newOrder.phone = createOrder.phone;
    newOrder.user = user;
    newOrder = await this.orderRepository.save(newOrder);

    const RENT_TIME = vipUser ? RENT_TIME_VIP : RENT_TIME_NORMAL; //days

    books.forEach(book => {
      book.stock -= 1;
    });

    await this.bookRepository.save(books);
    for (let i = 0; i < books.length; i++) {
      const newOrderBook = new OrderBook();
      newOrderBook.order = newOrder;
      newOrderBook.book = books[i];
      newOrderBook.rentedTime = new Date();
      newOrderBook.expirationTime = add(new Date(), { days: RENT_TIME });
      newOrder.orderBooks.push(newOrderBook);
    }

    newOrder.orderBooks = await this.orderBookRepository.save(newOrder.orderBooks);
    newOrder.orderBooks.forEach(items => {
      delete items.order;
    });

    return newOrder;
  }

  public async findAll(userId: number): Promise<Order[]> {
    const order = await this.orderRepository
      .createQueryBuilder('order')
      .leftJoin('order.user', 'user')
      .leftJoinAndSelect('order.orderBooks', 'order_book')
      .leftJoinAndSelect('order_book.book', 'book')
      .addSelect(['user.lastName', 'user.firstName', 'user.email', 'user.phone', 'user.avatar'])
      .where('user.id=:id', { id: userId })
      .getMany();

    return order;
  }

  public async findOne(userId: number, orderId: number | string): Promise<Order> {
    const order = await this.orderRepository
      .createQueryBuilder('order')
      .leftJoin('order.user', 'user')
      .leftJoinAndSelect('order.orderBooks', 'order_book')
      .leftJoinAndSelect('order_book.book', 'book')
      .addSelect(['user.lastName', 'user.firstName', 'user.email', 'user.phone', 'user.avatar'])
      .where('user.id=:userId', { userId })
      .andWhere('order.id=:orderId', { orderId })
      .getOne();
    if (!order) {
      throw new NotFoundException('findOne', 'Order not found');
    }
    return order;
  }

  public async findAllForAdmin(params: GetAllOrdersParamsDto): Promise<PaginationDto<Order>> {
    const { page, limit, search, sort, status } = params;

    const qb = this.orderRepository
      .createQueryBuilder('order')
      .leftJoin('order.user', 'user')
      .addSelect([
        'user.id',
        'user.firstName',
        'user.lastName',
        'user.email',
        'user.phone',
        'user.avatar',
        'user.role',
        'user.createdAt',
        'user.updatedAt',
      ])
      .leftJoinAndSelect('order.orderBooks', 'orderBooks')
      .leftJoinAndSelect('orderBooks.book', 'book')
      .take(limit)
      .skip((page - 1) * limit)
      .orderBy(SortParams[sort][0], SortParams[sort][1]);

    if (search) {
      qb.andWhere(
        new Brackets(sqb => {
          sqb.where('user.firstName LIKE :search', { search: `%${search}%` });
          sqb.orWhere('user.lastName LIKE :search', { search: `%${search}%` });
          sqb.orWhere('order.name LIKE :search', { search: `%${search}%` });
        }),
      );
    }
    //filter check state of order
    //every book in the same order has the same expirationTime
    //1: pending
    //2: received
    if (status) {
      switch (status) {
        case 1:
          qb.andWhere('orderBooks.status = :status', {status: OrderBookStatus.PENDING});
          break;

        case 2:
          qb.andWhere('orderBooks.status <> :status', {status: OrderBookStatus.PENDING});
          break;
      }
    }

    const result = await qb.getManyAndCount();
    return this.paginationService.paginate(params, result[0], result[1]);
  }

  public async findAllByUserId(userId: number | string): Promise<Order[]> {
    const orders = await this.orderRepository
      .createQueryBuilder('order')
      .leftJoin('order.user', 'user')
      .leftJoinAndSelect('order.orderBooks', 'orderbooks')
      .leftJoinAndSelect('orderbooks.book', 'book')
      .where('user.id =:userId', { userId: userId })
      .getMany();
    return orders;
  }

  public async receiveOrder(userId: number, orderId: number | string): Promise<Order> {
    const order = await this.orderRepository
      .createQueryBuilder('order')
      .leftJoinAndSelect('order.orderBooks', 'orderbooks')
      .where('order.id=:orderId', { orderId: orderId })
      .getOne();
    if (!order) {
      throw new NotFoundException('receivedOrder', 'Order not found');
    }
    if (order.orderBooks[0].status == OrderBookStatus.RECEIVED) {
      throw new BadRequestException('receivedOrder', "you've already received this order");
    }
    await this.orderBookRepository.update(
      { order: order, status: OrderBookStatus.PENDING },
      { receivedTime: new Date(), status: OrderBookStatus.RECEIVED },
    );

    return this.orderRepository
      .createQueryBuilder('order')
      .leftJoinAndSelect('order.orderBooks', 'orderbooks')
      .leftJoinAndSelect('orderbooks.book', 'book')
      .where('order.id=:orderId', { orderId: orderId })
      .getOne();
  }

  public async returningBooks(
    userId: number,
    returningBooksDto: ReturningOrderDto,
  ): Promise<OrderBook[]> {
    const orderbooks = await this.orderBookRepository
      .createQueryBuilder('orderbooks')
      .leftJoinAndSelect('orderbooks.order', 'order')
      .leftJoin('order.user', 'user')
      .leftJoinAndSelect('orderbooks.book', 'book')
      .where('user.id=:userId', { userId: userId })
      .andWhere('book.id IN (:...bookIds)', { bookIds: returningBooksDto.bookId })
      .andWhere('orderbooks.status=:status', { status: OrderBookStatus.RECEIVED })
      .getMany();

    if (orderbooks.length < returningBooksDto.bookId.length) {
      throw new NotFoundException('returning books', 'Orderbooks not found');
    }

    orderbooks.forEach(book => {
      book.status = OrderBookStatus.RETURNING;
    });

    await this.orderBookRepository.save(orderbooks);
    return orderbooks;
  }

  public async returnedBooks(returnedBooksDto: ReturnedBooksDto): Promise<OrderBook[]> {
    const bookIds = [];
    const orderIds = [];
    returnedBooksDto.items.forEach(item => {
      bookIds.push(item.bookId);
      orderIds.push(item.orderId);
    });

    const qb = this.orderBookRepository
      .createQueryBuilder('orderbooks')
      .leftJoinAndSelect('orderbooks.order', 'order')
      .leftJoinAndSelect('orderbooks.book', 'book')
      .leftJoin('order.user', 'user')
      .leftJoinAndSelect('user.account', 'account')
      .addSelect([
        'user.id',
        'user.firstName',
        'user.lastName',
        'user.avatar',
        'user.email',
        'user.phone',
        'user.role',
      ]);

    qb.where(
      new Brackets(qb => {
        for (let i = 0; i < bookIds.length; i++) {
          qb.orWhere(
            `order.id=${mysql.escape(orderIds[i])} AND book.id=${mysql.escape(bookIds[i])}`,
            {},
          );
        }
      }),
    );

    qb.andWhere('orderbooks.status=:status', { status: OrderBookStatus.RETURNING });
    const [orderbooks, count] = await qb.getManyAndCount();
    if (count < bookIds.length) {
      throw new NotFoundException('returned book', 'Orderbook not found');
    }

    //update state
    const books = [];
    const numOfBooks = new Map();
    const foulTimesofBooks = new Map();
    const accounts = [];

    //count quantily of each book
    orderbooks.forEach(orderbook => {
      const id = orderbook.book.id;
      if (numOfBooks.has(id)) {
        numOfBooks.set(id, numOfBooks.get(id) + 1);
      } else {
        numOfBooks.set(id, 1);
      }
    });

    //count foulTimes
    for (let i = 0; i < bookIds.length; i++) {
      const accountId = orderbooks[i].order.user.account.id;

      if (!foulTimesofBooks.has(accountId)) {
        foulTimesofBooks.set(accountId, 0);
      }

      const isLate = compareAsc(new Date(), orderbooks[i].expirationTime);

      if (isLate !== -1) {
        const diff = differenceInDays(new Date(), orderbooks[i].expirationTime);
        if (diff >= LATE_DAYS) {
          foulTimesofBooks.set(accountId, foulTimesofBooks.get(accountId) + 1);
        }
      }
    }

    for (let i = 0; i < bookIds.length; i++) {
      //update time confirm return
      orderbooks[i].status = OrderBookStatus.RETURNED;
      orderbooks[i].returnedTime = new Date();

      //update stock
      const id = orderbooks[i].book.id;
      orderbooks[i].book.stock += numOfBooks.get(id);
      books.push(orderbooks[i].book);

      //update foulTimes
      const accountId = orderbooks[i].order.user.account.id;
      orderbooks[i].order.user.account.foulTimes += foulTimesofBooks.get(accountId);
      accounts.push(orderbooks[i].order.user.account);
    }

    await this.accountRepository.save(accounts);
    await this.bookRepository.save(books);
    return this.orderBookRepository.save(orderbooks);
  }
}
