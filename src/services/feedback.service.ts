import { Service, Inject } from 'typedi';
import { Repository } from 'typeorm';

import { Feedback } from '@src/entities/Feedback';
import { CreateFeedbackDto } from '@src/dto/feedback.dto';
import { convertDto } from '@src/utils/common';
import { NotFoundException } from '@src/utils/CustomError';

@Service()
export default class FeedbackService {
  constructor(@Inject('feedbackRepository') private feedbackRepository: Repository<Feedback>) {}

  public async findAll(): Promise<Feedback[]> {
    return this.feedbackRepository.find();
  }

  public async findOne(id: number | string): Promise<Feedback> {
    const feedback = await this.feedbackRepository.findOne(id);
    if (!feedback) {
      throw new NotFoundException('findOneFeedback', 'Feedback not found');
    }
    return feedback;
  }

  public async create(createFeedbackDto: CreateFeedbackDto): Promise<Feedback> {
    const newFeedback = new Feedback();
    convertDto(createFeedbackDto, newFeedback);
    return this.feedbackRepository.save(newFeedback);
  }
}
