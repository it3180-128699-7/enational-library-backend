import { Service, Inject } from 'typedi';
import { Repository } from 'typeorm';

import { NotFoundException } from '@src/utils/CustomError';
import { Author } from '@src/entities/Author';
import { CreateAuthorDto, UpdateAuthorDto } from '@src/dto/author.dto';
import { convertDto } from '@src/utils/common';

@Service()
export default class AuthorService {
  constructor(@Inject('authorRepository') private authorRepository: Repository<Author>) {}

  public async findAll(): Promise<Author[]> {
    return this.authorRepository.find();
  }

  public async findOne(id: number | string): Promise<Author> {
    const author = await this.authorRepository.findOne(id);
    if (!author) {
      throw new NotFoundException('findOneAuthor', 'Author not found');
    }
    return author;
  }

  public async create(createAuthorDto: CreateAuthorDto): Promise<Author> {
    const newAuthor = new Author();
    convertDto(createAuthorDto, newAuthor);
    return this.authorRepository.save(newAuthor);
  }

  public async update(id: number | string, updateAuthorDto: UpdateAuthorDto): Promise<Author> {
    const author = await this.authorRepository.findOne(id);
    if (!author) {
      throw new NotFoundException('updateAuthor', 'Author not found');
    }
    convertDto(updateAuthorDto, author);
    return this.authorRepository.save(author);
  }

  public async delete(id: number | string): Promise<any> {
    const author = await this.authorRepository.findOne(id);
    if (!author) {
      throw new NotFoundException('delete author', 'author not found');
    }
    return this.authorRepository.delete(id);
  }
}
