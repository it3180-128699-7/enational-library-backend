import { Service, Inject } from 'typedi';
import { Repository } from 'typeorm';
import { compareAsc, add, differenceInCalendarMonths } from 'date-fns';

import { BadRequestException, NotFoundException } from '@src/utils/CustomError';
import { User } from '@src/entities/User';
import { Account } from '@src/entities/Account';
import { AddMoneyDto, RenewalDto } from '@src/dto/account.dto';
import config from '@src/config';

const {
  DISCOUNT_NORMAL_USER,
  DISCOUNT_VIP_USER,
  RENEWAL_NORMAL_USER_MONTH,
  RENEWAL_VIP_USER_MONTH,
} = config.appConstants;

@Service()
export default class AccountService {
  constructor(
    @Inject('accountRepository') private accountRepository: Repository<Account>,
    @Inject('userRepository') private userRepository: Repository<User>,
  ) {}

  public async addMoney(addMoney: AddMoneyDto): Promise<User> {
    const user = await this.userRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.account', 'account')
      .where('user.id=:userId', { userId: addMoney.userId })
      .getOne();
    if (!user) {
      throw new NotFoundException('addMoney', 'user not found');
    }

    const newBalance = user.account.balance + addMoney.money;
    user.account.balance = newBalance;
    this.accountRepository.update({ user: user }, { balance: newBalance });
    return user;
  }

  public async renewal(user: User, renewalDto: RenewalDto): Promise<User> {
    const checkexp = compareAsc(new Date(), user.account.expirationTime);
    let newBalance: number;
    let newExpirationTime: Date;
    const DISCOUNT = renewalDto.isVip ? 1 - DISCOUNT_VIP_USER : 1 - DISCOUNT_NORMAL_USER;
    const FEE = renewalDto.isVip
      ? RENEWAL_VIP_USER_MONTH * renewalDto.month
      : RENEWAL_NORMAL_USER_MONTH * renewalDto.month;
    newBalance =
      renewalDto.month >= 6 ? user.account.balance - FEE * DISCOUNT : user.account.balance - FEE;

    //account is not expired
    if (checkexp !== 1) {
      //norm account
      if (!user.account.isVip) {
        const remainTime = differenceInCalendarMonths(user.account.expirationTime, new Date());
        //norm to vip
        if (renewalDto.isVip) {
          newBalance -= RENEWAL_VIP_USER_MONTH * remainTime;
        }
        //add month for normal account
        if (!renewalDto.isVip) {
          newBalance -= RENEWAL_NORMAL_USER_MONTH * remainTime;
        }
        newExpirationTime = add(user.account.expirationTime, { months: renewalDto.month });
      }
      //vip account
      if (user.account.isVip) {
        //vip to norm
        if (!renewalDto.isVip) {
          throw new BadRequestException(
            'renewal',
            'your account is vip, cannot change to normal account until your account is expire',
          );
        }
        newExpirationTime = add(user.account.expirationTime, { months: renewalDto.month });
      }
    } else {
      // account is expired
      //renewal
      newExpirationTime = add(new Date(), { months: renewalDto.month });
    }

    if (newBalance >= 0) {
      await this.accountRepository.update(
        { user: user },
        { balance: newBalance, isVip: renewalDto.isVip, expirationTime: newExpirationTime },
      );
    } else {
      throw new BadRequestException('renewal', 'balance is not enough');
    }

    return this.userRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.account', 'account')
      .where('user.id=:userId', { userId: user.id })
      .getOne();
  }
}
