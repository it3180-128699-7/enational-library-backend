import { Service, Inject, Container } from 'typedi';
import { Brackets, Repository } from 'typeorm';

import { User, UserRole } from '@src/entities/User';
import { Account } from '@src/entities/Account';
import {
  BlockAndUnblockUserDto,
  GetAllUsersParamsDto,
  SortParams,
  UpdateAvatarDto,
  UpdateRoleDto,
  UpdateUserDto,
  UserViewDto,
} from '@src/dto/user.dto';
import { BadRequestException, NotFoundException } from '@src/utils/CustomError';
import { convertDto } from '@src/utils/common';
import PaginationService from './pagination.service';
import { PaginationDto } from '@src/dto/pagination.dto';

@Service()
export default class UserService {
  constructor(
    @Inject('userRepository') private userRepository: Repository<User>,
    @Inject('accountRepository') private accountRepository: Repository<Account>,
    private readonly paginationService = Container.get(PaginationService),
  ) {}

  public async updateUser(
    userId: number | string,
    updateUserDto: UpdateUserDto | UpdateAvatarDto,
  ): Promise<UserViewDto> {
    const user = await this.userRepository.findOne(userId);
    if (!user) {
      throw new NotFoundException('updateUser', 'User not found');
    }
    convertDto(updateUserDto, user);
    return this.userRepository.save(user);
  }

  public async blockAndUnBlockUser(
    blockAndUnblockUserDto: BlockAndUnblockUserDto,
  ): Promise<UserViewDto> {
    const user = await this.findOne(blockAndUnblockUserDto.userId);

    if (!user) {
      throw new NotFoundException('Block and unblock users', 'User not found');
    }

    const state = blockAndUnblockUserDto.isActive;

    //blocked account
    if (!user.account.isActive) {
      //block
      if (!state) {
        throw new BadRequestException('Block and unblock users', 'User is already blocked');
      }
      //unblock
      else {
        user.account.isActive = state;
      }
    } else {
      //active account
      //block
      if (!state) {
        user.account.isActive = state;
      } else {
        //not block
        throw new BadRequestException('Block and unblock users', 'User was not block');
      }
    }

    await this.accountRepository.save(user.account);

    return user;
  }

  public async updateRole(updateRoleDto: UpdateRoleDto): Promise<UserViewDto> {
    const user = await this.findOne(updateRoleDto.userId);

    if (!user) {
      throw new NotFoundException('UpdateRole', 'User not found');
    }

    let role;
    Object.keys(UserRole).filter(key => {
      if (UserRole[key].toString().toLowerCase() === updateRoleDto.role.toString().toLowerCase()) {
        role = UserRole[key];
      }
    });

    if (!role) {
      throw new BadRequestException('UpdateRole', 'Invalid role');
    }

    if (user.role === role) {
      throw new BadRequestException('UpdateRole', 'User already has this role ');
    }

    user.role = role;

    return this.userRepository.save(user);
  }

  public async findAllUsers(params: GetAllUsersParamsDto): Promise<PaginationDto<User>> {
    const { page, limit, search, sort, role, isActive, isVip, isExpired, foulTimes } = params;

    const qb = this.userRepository
      .createQueryBuilder('user')
      .select([
        'user.id',
        'user.firstName',
        'user.lastName',
        'user.email',
        'user.phone',
        'user.avatar',
        'user.role',
        'user.createdAt',
        'user.updatedAt',
      ])
      .leftJoinAndSelect('user.account', 'account')
      .take(limit)
      .skip((page - 1) * limit)
      .orderBy(SortParams[sort][0], SortParams[sort][1]);

    if (search) {
      qb.andWhere(
        new Brackets(sqb => {
          sqb.where('user.firstName LIKE :search', { search: `%${search}%` });
          sqb.orWhere('user.lastName LIKE :search', { search: `%${search}%` });
        }),
      );
    }

    //filter role
    //1: is admin users
    //2: is staff users
    //3: is member users
    //else : all users
    switch (role) {
      case 1:
        qb.andWhere('user.role =:role', { role: UserRole.ADMIN });
        break;
      case 2:
        qb.andWhere('user.role =:role', { role: UserRole.STAFF });
        break;
      case 3:
        qb.andWhere('user.role =:role', { role: UserRole.MEMBER });
        break;
    }

    //filter isActive
    //1: acitve users
    //2: blocked users
    //other: all users
    switch (isActive) {
      case 1:
        qb.andWhere('account.isActive = true');
        break;
      case 2:
        qb.andWhere('account.isActive = false');
        break;
    }

    //filter isVip users
    //1: vip users
    //2: normal users
    //other: all users
    switch (isVip) {
      case 1:
        qb.andWhere('account.isVip = true');
        break;
      case 2:
        qb.andWhere('account.isVip = false');
        break;
    }

    //filter expired account
    //1: exprired
    //2: not expired
    //other
    switch (isExpired) {
      case 1:
        qb.andWhere('account.expirationTime < CURRENT_TIMESTAMP() ');
        break;
      case 2:
        qb.andWhere('account.expirationTime >= CURRENT_TIME() ');
    }

    //filter foulTimes account
    //1 : foulTimes = 0
    //2 : foulTimes >= 1
    //3 : foulTimes >=3
    //other
    switch (foulTimes) {
      case 1:
        qb.andWhere('account.foulTimes = :a', { a: 0 });
        break;
      case 2:
        qb.andWhere('account.foulTimes >= :a', { a: 1 });
        break;
      case 3:
        qb.andWhere('account.foulTimes >= :a', { a: 3 });
        break;
    }

    const result = await qb.getManyAndCount();
    return this.paginationService.paginate(params, result[0], result[1]);
  }

  public async findOne(id: number | string): Promise<UserViewDto> {
    const user = this.userRepository
      .createQueryBuilder('user')
      .select([
        'user.id',
        'user.firstName',
        'user.lastName',
        'user.email',
        'user.phone',
        'user.avatar',
        'user.role',
        'user.createdAt',
        'user.updatedAt',
      ])
      .leftJoinAndSelect('user.account', 'account')
      .where('user.id = :id', { id })
      .getOne();

    return user;
  }
}
