import {
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';

import { OrderBook } from './OrderBook';
import { User } from './User';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  name: string;

  @Column()
  address: string;

  @Column()
  phone: string;

  @ManyToOne(() => User, user => user.orders)
  user: User;

  @OneToMany(() => OrderBook, orderBook => orderBook.order)
  orderBooks: OrderBook[];

  @CreateDateColumn({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP(6)' })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'datetime',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedAt: Date;
}
