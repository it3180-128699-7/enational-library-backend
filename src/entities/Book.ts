import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
  DeleteDateColumn,
} from 'typeorm';

import { Author } from './Author';
import { Category } from './Category';
import { OrderBook } from './OrderBook';

@Entity()
export class Book {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 255, unique: true })
  sku: string;

  @Column({ length: 255 })
  title: string;

  @Column({ length: 500 })
  image: string;

  @Column({ type: 'tinytext', nullable: true })
  description: string;

  @Column({ type: 'longtext' })
  content: string;

  @ManyToOne(() => Author, author => author.books)
  author: Author;

  @Column({ type: 'int' })
  pageNumber: number;

  @Column({ type: 'int', default: 9999 })
  stock: number;

  @Column({ default: false })
  isPublished: boolean;

  @Column({ type: 'datetime' })
  publishedDate: Date;

  @Column({ length: 255 })
  publisher: string;

  @Column({ default: false })
  isExclusive: boolean;

  @ManyToOne(() => Category, category => category.books)
  category: Category;

  @OneToMany(() => OrderBook, orderBook => orderBook.book)
  orderBooks: OrderBook[];

  @CreateDateColumn({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP(6)' })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'datetime',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
