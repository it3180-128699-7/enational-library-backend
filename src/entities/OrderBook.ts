import { Entity, CreateDateColumn, ManyToOne, Column } from 'typeorm';

import { Book } from './Book';
import { Order } from './Order';

export enum OrderBookStatus {
  PENDING = 'pending',
  RECEIVED = 'received',
  RETURNING = 'returning',
  RETURNED = 'returned',
}

@Entity()
export class OrderBook {
  @ManyToOne(() => Order, order => order.orderBooks, { primary: true })
  order: Order;

  @ManyToOne(() => Book, book => book.orderBooks, { primary: true })
  book: Book;

  @Column({
    type: 'enum',
    enum: OrderBookStatus,
    default: OrderBookStatus.PENDING,
  })
  status: OrderBookStatus;

  @CreateDateColumn({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP(6)' })
  rentedTime: Date;

  @Column({ type: 'datetime', nullable: true })
  receivedTime: Date;

  @Column({ type: 'datetime', nullable: true })
  expirationTime: Date;

  @Column({ type: 'datetime', nullable: true })
  returnedTime: Date;
}
