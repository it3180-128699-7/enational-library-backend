import attachCurrentUser from './attachCurrentUser';
import { checkRole, checkPermission } from './checkRole';
import isAuth from './isAuth';
import isOptionalAuth from './isOptionalAuth';
import { uploadImageByDisk, uploadImageByMemory } from './multerUpload';

export default {
  attachCurrentUser,
  isAuth,
  isOptionalAuth,
  checkRole,
  checkPermission,
  uploadImageByMemory,
  uploadImageByDisk,
};
