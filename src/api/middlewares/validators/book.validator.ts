import { celebrate, Joi } from 'celebrate';

export default {
  create: celebrate({
    body: Joi.object({
      sku: Joi.string().required(),
      title: Joi.string().required(),
      image: Joi.string().uri().required(),
      description: Joi.string(),
      content: Joi.string().required(),
      author: Joi.number().required(),
      pageNumber: Joi.number().required(),
      isPublished: Joi.boolean().default(false),
      stock: Joi.number(),
      publishedDate: Joi.date().iso().required(),
      publisher: Joi.string().required(),
      isExclusive: Joi.boolean().default(false),
      category: Joi.number().required(),
    }),
  }),

  update: celebrate({
    body: Joi.object({
      sku: Joi.string().required(),
      title: Joi.string().required(),
      image: Joi.string().uri().required(),
      description: Joi.string(),
      content: Joi.string().required(),
      author: Joi.number().required(),
      pageNumber: Joi.number().required(),
      isPublished: Joi.boolean(),
      stock: Joi.number(),
      publishedDate: Joi.date().iso().required(),
      publisher: Joi.string().required(),
      isExclusive: Joi.boolean(),
      category: Joi.number().required(),
    }),
  }),
};
