import { celebrate, Joi } from 'celebrate';

export default {
  create: celebrate({
    body: Joi.object({
      name: Joi.string().required(),
      phone: Joi.string().required(),
      address: Joi.string().required(),
      books: Joi.array().items(Joi.number()).required(),
    }),
  }),

  returningBook: celebrate({
    body: Joi.object({
      bookId: Joi.array().items(Joi.number()).required(),
    }),
  }),

  returnedBook: celebrate({
    body: Joi.object({
      items: Joi.array()
        .items(
          Joi.object({
            orderId: Joi.number(),
            bookId: Joi.number(),
          }),
        )
        .required(),
    }),
  }),
};
