import { celebrate, Joi } from 'celebrate';

import { VIETNAMESE_PHONE_REGEX } from '@src/config/constants';

export default {
  signUp: celebrate({
    body: Joi.object({
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      phone: Joi.string().regex(VIETNAMESE_PHONE_REGEX).message('Invalid Vietnamese phone number'),
      avatar: Joi.string(),
      password: Joi.string().min(8).required(),
    }),
  }),

  signIn: celebrate({
    body: Joi.object({
      email: Joi.string().required(),
      password: Joi.string().required(),
      remember: Joi.boolean(),
    }),
  }),

  changePassword: celebrate({
    body: Joi.object({
      oldPassword: Joi.string().required(),
      newPassword: Joi.string().min(8).required(),
    }),
  }),
};
