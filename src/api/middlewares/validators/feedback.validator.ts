import { celebrate, Joi } from 'celebrate';
import { VIETNAMESE_PHONE_REGEX } from '@src/config/constants';

export default {
  create: celebrate({
    body: {
      name: Joi.string().required(),
      email: Joi.string().email().required(),
      content: Joi.string().required(),
      phone: Joi.string().regex(VIETNAMESE_PHONE_REGEX).message('Invalid Vietnamese phone number'),
    },
  }),
};
