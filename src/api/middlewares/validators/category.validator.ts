import { celebrate, Joi } from 'celebrate';

export default {
  create: celebrate({
    body: Joi.object({
      title: Joi.string().required(),
      description: Joi.string(),
    }),
  }),

  update: celebrate({
    body: Joi.object({
      title: Joi.string().required(),
      description: Joi.string(),
    }),
  }),
};
