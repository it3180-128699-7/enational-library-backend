import { celebrate, Joi } from 'celebrate';
export default {
  create: celebrate({
    body: Joi.object({
      name: Joi.string().required(),
      bio: Joi.string(),
      avatar: Joi.string().required(),
    }),
  }),

  update: celebrate({
    body: Joi.object({
      name: Joi.string().required(),
      bio: Joi.string(),
      avatar: Joi.string().required(),
    }),
  }),
};
