import { celebrate, Joi } from 'celebrate';

export default {
  renewal: celebrate({
    body: Joi.object({
      month: Joi.number().integer().min(0).required(),
      isVip: Joi.boolean().required(),
    }),
  }),

  deposit: celebrate({
    body: Joi.object({
      userId: Joi.number().required(),
      money: Joi.number().min(0).required(),
    }),
  }),
};
