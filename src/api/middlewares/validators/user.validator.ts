import { celebrate, Joi } from 'celebrate';

export default {
  update: celebrate({
    body: Joi.object({
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
    }),
  }),

  updateAvatar: celebrate({
    body: Joi.object({
      avatar: Joi.string().uri().required(),
    }),
  }),

  updateRole: celebrate({
    body: Joi.object({
      userId: Joi.number().min(1).required(),
      role: Joi.string().required(),
    }),
  }),

  blockAndUnblockUser: celebrate({
    body: Joi.object({
      userId: Joi.number().required(),
      isActive: Joi.boolean().required(),
    }),
  }),
};
