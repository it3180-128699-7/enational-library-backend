import { Router } from 'express';

import authApi from './routes/auth.route';
import imageApi from './routes/image.route';
import userApi from './routes/user.route';
import categoryApi from './routes/category.route';
import authorApi from './routes/author.route';
import feedbackApi from './routes/feedback.route';
import bookApi from './routes/book.route';
import accountApi from './routes/account.route';
import setupSwagger from './setup-swagger';
import orderApi from './routes/order.route';

export default () => {
  const app = Router();

  setupSwagger(app);

  authApi(app);
  imageApi(app);
  userApi(app);
  categoryApi(app);
  authorApi(app);
  feedbackApi(app);
  bookApi(app);
  accountApi(app);
  orderApi(app);

  return app;
};
