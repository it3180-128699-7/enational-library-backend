import { Router, Request, Response, NextFunction } from 'express';
import Container from 'typedi';

import BookService from '@src/services/book.service';
import middlewares from '@src/api/middlewares';
import { bookValidator } from '@src/api/middlewares/validators';
import { UserRole } from '@src/entities/User';
import { CreateBookDto, GetAllBooksParamsDto, UpdateBookDto } from '@src/dto/book.dto';

const route = Router();

export default (app: Router) => {
  app.use('/books', route);

  route.get(
    '/',
    middlewares.isOptionalAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    async (req: Request, res: Response, next: NextFunction) => {
      const canViewNoPublished = req.hasPermission;
      let { page, limit, search, sort, exclusive, author, category } =
        req.query as GetAllBooksParamsDto;
      (page = +page || 1),
        (limit = +limit || 100),
        (search = search || ''),
        (sort = +sort || 2),
        (exclusive = +exclusive),
        (author = +author || null),
        (category = +category || null);

      try {
        const bookService = Container.get(BookService);
        const result = await bookService.findAll(
          { page, limit, search, sort, exclusive, author, category },
          canViewNoPublished,
        );
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.get(
    '/:id',
    middlewares.isOptionalAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const canViewNoPublished = req.hasPermission;
        const bookService = Container.get(BookService);
        const result = await bookService.findOne(req.params.id, canViewNoPublished);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.post(
    '/',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    bookValidator.create,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const bookService = Container.get(BookService);
        const result = await bookService.create(req.body as CreateBookDto);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.put(
    '/:id',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    bookValidator.update,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const bookService = Container.get(BookService);
        const result = await bookService.update(req.params.id, req.body as UpdateBookDto);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.delete(
    '/:id',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const bookService = Container.get(BookService);
        const result = await bookService.delete(req.params.id);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );
};
