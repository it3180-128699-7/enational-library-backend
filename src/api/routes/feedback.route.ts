import { Router, Request, Response, NextFunction } from 'express';
import Container from 'typedi';

import FeedbackService from '@src/services/feedback.service';
import { feedbackValidator } from '@src/api/middlewares/validators';
import { CreateFeedbackDto } from '@src/dto/feedback.dto';
import { UserRole } from '@src/entities/User';
import middlewares from '@src/api/middlewares';

const route = Router();

export default (app: Router) => {
  app.use('/feedbacks', route);

  route.get(
    '/',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const feedbackService = Container.get(FeedbackService);
        const result = await feedbackService.findAll();
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.get(
    '/:id',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const feedbackService = Container.get(FeedbackService);
        const result = await feedbackService.findOne(req.params.id);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.post(
    '/',
    feedbackValidator.create,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const feedbackService = Container.get(FeedbackService);
        const result = await feedbackService.create(req.body as CreateFeedbackDto);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );
};
