import { Router, Request, Response, NextFunction } from 'express';
import Container from 'typedi';

import middlewares from '@src/api/middlewares';
import UserService from '@src/services/user.service';
import { userValidators } from '@src/api/middlewares/validators';
import {
  BlockAndUnblockUserDto,
  GetAllUsersParamsDto,
  UpdateAvatarDto,
  UpdateRoleDto,
  UpdateUserDto,
} from '@src/dto/user.dto';
import _ from 'lodash';
import { UserRole } from '@src/entities/User';

const route = Router();

export default (app: Router) => {
  app.use('/users', route);

  route.get(
    '/me',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    (req: Request, res: Response) => {
      return res.status(200).json(req.currentUser);
    },
  );

  route.put(
    '/me',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    userValidators.update,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const userService = Container.get(UserService);
        const user = await userService.updateUser(req.currentUser.id, req.body as UpdateUserDto);
        return res.status(200).json(_.omit(user, ['password', 'salt']));
      } catch (err) {
        next(err);
      }
    },
  );

  route.put(
    '/avatar',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    userValidators.updateAvatar,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const userService = Container.get(UserService);
        const user = await userService.updateUser(req.currentUser.id, req.body as UpdateAvatarDto);
        return res.status(200).json(_.omit(user, ['password', 'salt']));
      } catch (err) {
        next(err);
      }
    },
  );

  route.put(
    '/block',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    userValidators.blockAndUnblockUser,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const userService = Container.get(UserService);
        const user = await userService.blockAndUnBlockUser(req.body as BlockAndUnblockUserDto);
        return res.status(200).json(_.omit(user, ['password', 'salt']));
      } catch (err) {
        next(err);
      }
    },
  );

  route.put(
    '/role',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN]),
    middlewares.checkPermission,
    userValidators.updateRole,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const userService = Container.get(UserService);
        const user = await userService.updateRole(req.body as UpdateRoleDto);
        return res.status(200).json(_.omit(user, ['password', 'salt']));
      } catch (err) {
        next(err);
      }
    },
  );

  route.get(
    '/',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    async (req: Request, res: Response, next: NextFunction) => {
      let { page, limit, search, sort, role, isActive, isVip, isExpired, foulTimes } =
        req.query as GetAllUsersParamsDto;
      (page = +page || 1),
        (limit = +limit || 100),
        (search = search || ''),
        (sort = +sort || 2),
        (role = +role),
        (isActive = +isActive),
        (isVip = +isVip),
        (isExpired = +isExpired),
        (foulTimes = +foulTimes);

      try {
        const userService = Container.get(UserService);
        const result = await userService.findAllUsers({
          page,
          limit,
          search,
          sort,
          role,
          isActive,
          isVip,
          isExpired,
          foulTimes,
        });
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );
};
