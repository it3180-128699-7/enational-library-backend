import { Router, Request, Response, NextFunction } from 'express';
import Container from 'typedi';
import _ from 'lodash';

import OrderService from '@src/services/order.service';
import midddlewares from '@src/api/middlewares';
import { orderBookValidator } from '@src/api/middlewares/validators';
import { User, UserRole } from '@src/entities/User';
import {
  CreateOrderDto,
  GetAllOrdersParamsDto,
  ReturnedBooksDto,
  ReturningOrderDto,
} from '@src/dto/order.dto';

const route = Router();

export default (app: Router) => {
  app.use('/orders', route);

  route.post(
    '/',
    midddlewares.isAuth,
    midddlewares.attachCurrentUser,
    orderBookValidator.create,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const orderService = Container.get(OrderService);
        const result = await orderService.create(
          req.currentUser as User,
          req.body as CreateOrderDto,
        );
        return res.status(200).json(_.omit(result, ['user.password', 'user.salt']));
      } catch (error) {
        next(error);
      }
    },
  );

  route.get(
    '/',
    midddlewares.isAuth,
    midddlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const orderService = Container.get(OrderService);
        const result = await orderService.findAll(req.currentUser.id);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.get(
    '/users',
    midddlewares.isAuth,
    midddlewares.attachCurrentUser,
    midddlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    midddlewares.checkPermission,
    async (req: Request, res: Response, next: NextFunction) => {
      let { page, limit, search, sort, status } = req.query as GetAllOrdersParamsDto;

      (page = +page || 1),
        (limit = +limit || 100),
        (search = search || ''),
        (sort = +sort || 2),
        (status = +status);

      try {
        const orderService = Container.get(OrderService);
        const result = await orderService.findAllForAdmin({
          page,
          limit,
          search,
          sort,
          status,
        });
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.get(
    '/users/:userId',
    midddlewares.isAuth,
    midddlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const orderService = Container.get(OrderService);
        const result = await orderService.findAllByUserId(req.params.userId);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.get(
    '/:orderId',
    midddlewares.isAuth,
    midddlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const orderService = Container.get(OrderService);
        const result = await orderService.findOne(req.currentUser.id, req.params.orderId);
        return res.status(200).json(_.omit(result, ['order.user.password', 'order.user.salt']));
      } catch (error) {
        next(error);
      }
    },
  );

  route.put(
    '/confirm-receive/:orderId',
    midddlewares.isAuth,
    midddlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const orderService = Container.get(OrderService);
        const result = await orderService.receiveOrder(req.currentUser.id, req.params.orderId);
        return res.status(200).json(_.omit(result, ['order.user.password', 'order.user.salt']));
      } catch (error) {
        next(error);
      }
    },
  );

  route.put(
    '/return',
    midddlewares.isAuth,
    midddlewares.attachCurrentUser,
    orderBookValidator.returningBook,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const orderService = Container.get(OrderService);
        const result = await orderService.returningBooks(
          req.currentUser.id,
          req.body as ReturningOrderDto,
        );
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.put(
    '/confirm-return',
    midddlewares.isAuth,
    midddlewares.attachCurrentUser,
    midddlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    midddlewares.checkPermission,
    orderBookValidator.returnedBook,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const orderService = Container.get(OrderService);
        const result = await orderService.returnedBooks(req.body as ReturnedBooksDto);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );
};
