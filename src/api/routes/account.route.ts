import { Router, Request, Response, NextFunction } from 'express';
import Container from 'typedi';
import _ from 'lodash';

import AccountService from '@src/services/account.service';
import middlewares from '@src/api/middlewares';
import { accountValidator } from '@src/api/middlewares/validators';
import { User, UserRole } from '@src/entities/User';
import { AddMoneyDto, RenewalDto } from '@src/dto/account.dto';

const route = Router();

export default (app: Router) => {
  app.use('/accounts', route);

  route.put(
    '/deposit/',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    accountValidator.deposit,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const account = Container.get(AccountService);
        const result = await account.addMoney(req.body as AddMoneyDto);
        return res.status(200).json(_.omit(result, ['password', 'salt']));
      } catch (error) {
        next(error);
      }
    },
  );

  route.put(
    '/renewal/',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    accountValidator.renewal,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const account = Container.get(AccountService);
        const result = await account.renewal(req.currentUser as User, req.body as RenewalDto);
        return res.status(200).json(_.omit(result, ['password', 'salt']));
      } catch (error) {
        next(error);
      }
    },
  );
};
