import { Router, Request, Response, NextFunction } from 'express';
import Container from 'typedi';

import CategoryService from '@src/services/category.service';
import middlewares from '@src/api/middlewares';
import categoryValidator from '@src/api/middlewares/validators/category.validator';
import { UserRole } from '@src/entities/User';
import { CreateCategoryDto, UpdateCategoryDto } from '@src/dto/category.dto';

const route = Router();

export default (app: Router) => {
  app.use('/categories', route);

  route.get('/', async (req: Request, res: Response, next: NextFunction) => {
    try {
      const categoryService = Container.get(CategoryService);
      const result = await categoryService.findAll();
      return res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  });

  route.get('/:id', async (req: Request, res: Response, next: NextFunction) => {
    try {
      const categoryService = Container.get(CategoryService);
      const result = await categoryService.findOne(req.params.id);
      return res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  });

  route.post(
    '/',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    categoryValidator.create,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const categoryService = Container.get(CategoryService);
        const result = await categoryService.create(req.body as CreateCategoryDto);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.put(
    '/:id',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    categoryValidator.update,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const categoryService = Container.get(CategoryService);
        const result = await categoryService.update(req.params.id, req.body as UpdateCategoryDto);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.delete(
    '/:id',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const categoryService = Container.get(CategoryService);
        const result = await categoryService.delete(req.params.id);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );
};
