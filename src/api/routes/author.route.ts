import { Router, Request, Response, NextFunction } from 'express';
import Container from 'typedi';

import AuthorService from '@src/services/author.service';
import middlewares from '@src/api/middlewares';
import { UserRole } from '@src/entities/User';
import { CreateAuthorDto, UpdateAuthorDto } from '@src/dto/author.dto';
import { authorValidator } from '@src/api/middlewares/validators';

const route = Router();

export default (app: Router) => {
  app.use('/authors', route);

  route.get('/', async (req: Request, res: Response, next: NextFunction) => {
    try {
      const authorService = Container.get(AuthorService);
      const result = await authorService.findAll();
      return res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  });

  route.get('/:id', async (req: Request, res: Response, next: NextFunction) => {
    try {
      const authorService = Container.get(AuthorService);
      const result = await authorService.findOne(req.params.id);
      return res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  });

  route.post(
    '/',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    authorValidator.create,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const authorService = Container.get(AuthorService);
        const result = await authorService.create(req.body as CreateAuthorDto);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.put(
    '/:id',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    authorValidator.update,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const authorService = Container.get(AuthorService);
        const result = await authorService.update(req.params.id, req.body as UpdateAuthorDto);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );

  route.delete(
    '/:id',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    middlewares.checkRole([UserRole.ADMIN, UserRole.STAFF]),
    middlewares.checkPermission,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const authorService = Container.get(AuthorService);
        const result = await authorService.delete(req.params.id);
        return res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    },
  );
};
