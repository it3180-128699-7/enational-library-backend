import { PaginationParamsDto } from './pagination.dto';

export interface ReturnedBookDto {
  bookId: number;
  orderId: number;
}

export interface CreateOrderDto {
  name: string;
  phone: string;
  address: string;
  books: number[];
}

export interface ReturningOrderDto {
  bookId: number[];
}

export interface ReturnedBooksDto {
  items: ReturnedBookDto[];
}

export interface GetAllOrdersParamsDto extends PaginationParamsDto {
  search?: string;
  sort?: number;
  status?: number;
}

export const SortParams = {
  1: ['order.createdAt', 'ASC'],
  2: ['order.createdAt', 'DESC'],
};
