import { PaginationParamsDto } from './pagination.dto';

export interface CreateBookDto {
  sku: string;
  title: string;
  image: string;
  description?: string;
  content: string;
  author: number;
  pageNumber: number;
  isPublished?: boolean;
  stock?: number;
  publishedDate: Date;
  publisher: string;
  isExclusive?: boolean;
  category: number;
}

export interface UpdateBookDto {
  sku: string;
  title: string;
  image: string;
  description?: string;
  content: string;
  author: number;
  pageNumber: number;
  isPublished?: boolean;
  stock?: number;
  publishedDate: Date;
  publisher: string;
  isExclusive?: boolean;
  category: number;
}

export interface GetAllBooksParamsDto extends PaginationParamsDto {
  search?: string;
  sort?: number;
  exclusive?: number;
  category?: number;
  author?: number;
}

export const SortParams = {
  1: ['book.createdAt', 'ASC'],
  2: ['book.createdAt', 'DESC'],
  3: ['book.title', 'ASC'],
  4: ['book.title', 'DESC'],
  5: ['popular', 'DESC'],
};
