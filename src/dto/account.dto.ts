export interface AddMoneyDto {
  userId: number;
  money: number;
}
export interface RenewalDto {
  month: number;
  isVip: boolean;
}
