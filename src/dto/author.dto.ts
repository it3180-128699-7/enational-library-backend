export interface CreateAuthorDto {
  name: string;
  avatar: string;
  bio?: string;
}

export interface UpdateAuthorDto {
  name: string;
  avatar: string;
  bio?: string;
}
