import { User } from '@src/entities/User';
import { PaginationParamsDto } from './pagination.dto';

export interface CreateUserDto {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  avatar: string;
  password: string;
}

export interface ChangePasswordDto {
  oldPassword: string;
  newPassword: string;
}

export interface UpdateUserDto {
  firstName: string;
  lastName: string;
  avatar?: string;
}

export interface UpdateAvatarDto {
  avatar: string;
}

export interface BlockAndUnblockUserDto {
  userId: number;
  isActive: boolean;
}

export interface UpdateRoleDto {
  userId: number;
  role: string;
}

export interface GetAllUsersParamsDto extends PaginationParamsDto {
  search?: string;
  sort?: number;
  role?: number;
  isActive?: number;
  isVip?: number;
  isExpired?: number;
  foulTimes?: number;
}

export const SortParams = {
  1: ['user.createdAt', 'ASC'],
  2: ['user.createdAt', 'DESC'],
  3: ['user.firstName', 'ASC'],
  4: ['user.firstName', 'DESC'],
};

export type UserViewDto = Omit<User, 'password' | 'salt'>;
