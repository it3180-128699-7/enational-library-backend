export interface CreateCategoryDto {
  title: string;
  description?: string;
}

export interface UpdateCategoryDto {
  title: string;
  description?: string;
}
