export interface CreateFeedbackDto {
  name: string;
  email: string;
  phone?: string;
  content: string;
}
