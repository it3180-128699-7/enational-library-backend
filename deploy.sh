#!/bin/bash

export PATH=~/.nvm/versions/node/v16.13.0/bin:$PATH
source ~/.profile

# Pull necessary files for preventing errors
git stash
git pull origin main

# Install dependencies
yarn install

# Check if the app was running.
pm2 delete -s enational-library-backend || :
pm2 start --name enational-library-backend "yarn start:prod"
