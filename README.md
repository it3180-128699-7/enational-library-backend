# ENational Library Backend

This is a base source code for continue developing other project.

This code still have some comments for explaination, remove them if not necessary.

The entities current has: User. All of theme have only some sample fields and relations so I could write a fews API for Signin and Signup (containning some middlewares).

---

## Main techs

- `Node.js` with `NPM`, `yarn` for dependencies management.
- `Typescript` language (support build to js files with absolute path).
- `Express` framework for building a RESTful API server.
- `Json web token` for authorization.
- `Typeorm`: ORM framework for work easier with database.
- `Mysql`: Popular RDBMS
- `Winston` for logger (already installed with Vietnamese timezone).
- `Swagger UI` for API docs (`Open API 3.0.1`).

---

## Getting started

### Clone repository

```
git clone git@github.com:gerpann/gerpan-express-typeorm-boilerplate.git
```

### Prepare

I used `node` version `16.13.0` (The `lts` version at the time I write this), but you can also install other `lts` version

- For installing `lts` version:
  ```bash
  nvm install --lts
  ```
- For specific version (example is `16.13.0`):
  ```bash
  nvm install 16.13.0
  ```

Custom our environment variables like `.env.example` file:

```bash
cp .env.example .env
```

then edit the `.env` file

I _recommend_ you to install `yarn` at global scope before install dependencies of this source code

```
npm install -g yarn
```

The first time, you will need to run this script to install dependencies.

```
yarn
```

For run with database (I used Mysql), you can config it in `.env` file and some files in folder `src/config`.

Normally, you have to create a schema before run any code relate with database.

---

## Generate tables for new (empty) schema

- Make sure your schema has just been created or does not have any tables.
- Next, open file `package.json` and find in the script `migration:generate` where `-n db` and replace them with your favourite name (don't think too much because this just for make migrations data)
- Then run script following script to generate script for create table in the future

```bash
yarn migration:generate
```

- After the previous step, you will see a file have name like `1625156147394-XXX.ts` (timestamp-yourname.ts), it contains data to make tables for schema.
- Finally, run the below script to apply this to your schema.

```bash
yarn migration:run
```

## Run project

### Run project in development

```bash
yarn start:dev
```

### Run project in production

Build project

```
yarn build
```

Run

```
yarn start:prod
```

_To see some other scripts you can open file `package.json` for detail._

## API docs

With your `$HOST` provided in `.env` file, the api docs should locate at `$HOST/docs`.

The default as my example: `http://localhost:4000/api/docs`.
